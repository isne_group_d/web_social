<?php
session_start();

// initializing variables
$username = "";
$email    = "";
$search = "";
$Firstname ="";
$Lastname ="";
$phone="";
$errors = array(); 

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'social_network');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
  $Firstname = mysqli_real_escape_string($db, $_POST['Firstname']);
  $Lastname = mysqli_real_escape_string($db, $_POST['Lastname']);
  $phone = mysqli_real_escape_string($db, $_POST['phone']);
  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
  array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database

    $query = "INSERT INTO users (username, email, password,First_name,Last_name,Mobile) 
          VALUES('$username', '$email', '$password','$Firstname','$Lastname','$phone')";
    mysqli_query($db, $query);
    $_SESSION['username'] = $username;
    $_SESSION['success'] = "You are now logged in";
    header('location: home.php');
  }
}

if (isset($_POST['login_user'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
    array_push($errors, "Username is required");
  }
  if (empty($password)) {
    array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
    $password = md5($password);
    $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
    $results = mysqli_query($db, $query);
    if (mysqli_num_rows($results) == 1) {
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in";
      header('location: home.php');
    }else {
      array_push($errors, "Wrong username/password combination");
    }
  }
}

if (isset($_POST['search_user'])) {
  $search = mysqli_real_escape_string($db, $_POST['search']);

    $query = "SELECT * FROM users WHERE username='$search'";
    $results = mysqli_query($db, $query);
    if (mysqli_num_rows($results) == 1) {
      $_SESSION['search'] = $search;
      header('location: search.php');
    }else {
      unset($_SESSION['search']);
      array_push($errors, "Not found");
    }
}

if (isset($_POST['add_user'])) {


  $usert=$_SESSION['username'];
  $search=$_SESSION['search'];
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$usert'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $OriginID = $row["id"];
    }
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$search'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $DestID = $row["id"];
    }


  $check = true;
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM addid where DestID = '$DestID' AND OriginID = '$OriginID'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $check = false;
    }
  if($check == true)
  {
    $link = "INSERT INTO addid (OriginID,DestID) VALUES ('$OriginID','$DestID')";
  $result = $conn->query($link);
  }

}

if (isset($_POST['change_password'])) {
  // receive all input values from the form
  $password_1 = mysqli_real_escape_string($db, $_POST['password']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
  array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database


    $usert=$_SESSION['username'];
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$usert'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $OriginID = $row["id"];
    }

    $sql="UPDATE users SET password = '$password' WHERE id = '$OriginID'";
    $result = $db->query($sql);
    header('location: home.php');
  }
}

if (isset($_POST['change_profile'])) {
  // receive all input values from the form
  $first_name = mysqli_real_escape_string($db, $_POST['first_name']);
  $last_name = mysqli_real_escape_string($db, $_POST['last_name']);
  $mobile = mysqli_real_escape_string($db, $_POST['mobile']);
  $email = mysqli_real_escape_string($db, $_POST['email']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  $user_check_query = "SELECT * FROM users WHERE username='$username' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database


    $usert=$_SESSION['username'];
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$usert'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $OriginID = $row["id"];
    }

    $sql="UPDATE users SET first_name = '$first_name' , last_name = '$last_name' , mobile = '$mobile' , email = '$email' WHERE id = '$OriginID'";
    $result = $db->query($sql);
    header('location: profile.php');
  }
}

if (isset($_POST['post'])) {


  $title=$_SESSION['title'];
  $detail=$_SESSION['detail'];
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$username'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $OriginID = $row["id"];
    }
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM users where username = '$search'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $DestID = $row["id"];
    }


  $check = true;
  $conn = new mysqli('localhost', 'root', '', 'social_network');
  $sql = "SELECT * FROM addid where DestID = '$DestID' AND OriginID = '$OriginID'";
  $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $check = false;
    }
  if($check == true)
  {
    $link = "INSERT INTO addid (OriginID,DestID) VALUES ('$OriginID','$DestID')";
  $result = $conn->query($link);
  }

}
?>
