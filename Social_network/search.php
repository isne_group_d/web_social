<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
<title>Search</title>
<link rel="stylesheet" href="styleh.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
</head>
<body>

<div class="topnav">
  <a class="active" href="home.php">Home</a>
  <a href="profile.php">Profile</a>
  <a href="#about">About</a>
  <div class="search-container">
    <form method="post" action="search.php">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit" name="search_user"><i class="fa fa-search"></i></button>
    </form>
  </div>
  <a href="index.php?logout='1'">logout</a> 
</div>

<div class="content">

<?php include('errors.php'); ?>
<?php  if (isset($_SESSION['search'])) : ?>
<section class="container mt-4 mb-4">
<div class="container">
  <div class="row mb-3">
    <div class="col-md-6">
      <div class="d-flex flex-row border rounded">
          <div class="p-0 w-25">
              <img src="https://c1.staticflickr.com/3/2862/12328317524_18e52b5972_k.jpg" class="img-thumbnail border-0" />
            
          </div>
          <div class="pl-3 pt-2 pr-2 pb-2 w-75 border-left">
              <h4 class="text-primary"><?php echo $_SESSION['search']; ?></h4>
              <h5 class="text-info">Photographer</h5>
              <ul class="m-0 float-left" style="list-style: none; margin:0; padding: 0">
                <li><i class="fab fa-facebook-square"></i> Facebook</li>
                <li><i class="fab fa-twitter-square"></i> Twitter</li>
              </ul>
              <form method="post" action="search.php">
            <button class="text-right m-0" name="add_user"><a class="btn btn-primary"><i class="far fa-user"></i> Add friends</a></button>
            </form>
        </div>
      </div>
    </div>
  </div>

</div>
</section>
    <?php endif ?>


</div>


</body>
</html>